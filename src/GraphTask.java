import java.util.*;

/**
 * Ülesandeks oli koostada meetod sidusa lihtgraafi G tsentri leidmiseks.
 * Kasutatud allikad olid:
 * http://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
 * Buldas, A., Laud, P., Willemson, J. Graafid, Teine, parandatud ja täiendatud trükk. Tartu ülikooli matemaatika- informaatikateaduskond, arvutiteaduse instituut, Tartu 2008, 111lk
 * http://mathworld.wolfram.com/GraphEccentricity.html
 * http://mathworld.wolfram.com/GraphDistance.html
 * https://en.wikipedia.org/wiki/Graph_center
 *
 * Koostaja: Martin Jääger, DK22, 05.04.2017
 * Juhendaja: Jaanus Pöial
 */


/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   Graph g = new Graph ("G");
   private long timedifference;

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      //g.createRandomSimpleGraph (6, 9);
      g.createRandomSimpleGraph(2000, 3000);
      //test1();
      //test2();
      //test3();
      //test4();
      //test5();
      //test6();
      if (g.getVertexList().size() < 50)
         System.out.println (g);

      // TODO!!! Your experiments here
      /**
       * Here the results are printed out from a returned Arraylist;
       */
      System.out.print("Graph center/centers are: ");
      for(Vertex s : getGraphCenter()){
         System.out.print(s.id + " ");
      }
      System.out.printf("%ngetGraphCenter execution time was : " + timedifference + " ms");
   }

   /**
    * Function that calls the calculation of eccentricity on all vertices and returns
    * an ArrayList containing all the centers of the graph.
    * @return centers
    */
   public ArrayList<Vertex> getGraphCenter(){
      long starttime, endtime;
      starttime = System.nanoTime();

      ArrayList<Vertex> comparables = g.getVertexList();
      ArrayList<Vertex> centers = new ArrayList<>();

      if (comparables.size() == 1) return comparables;
      Vertex mineccentricity = null;

      for (int i = 0; i < comparables.size(); i++) {
         if (mineccentricity == null)
            mineccentricity = comparables.get(i);
         if (g.breadthFirst(comparables.get(i)) < g.breadthFirst(mineccentricity)){
            mineccentricity = comparables.get(i);
         }
      }

      for (int i = 0; i < comparables.size(); i++) {
         if (g.breadthFirst(comparables.get(i)) == g.breadthFirst(mineccentricity)){
            centers.add(comparables.get(i));
         }
      }
      endtime = System.nanoTime();
      timedifference = ((endtime-starttime)/1000000);
      return centers;
   }

   /**
    * The vertex class that handles references to relevant qualities and other graph objects.
    * Added function to get all connected edges and getters/setters.
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0; //for keeping data concerning the relevant status of the vertex.
      // You can add more fields, if needed
      private int distance; //for keeping data concerning distance from specific vertex.

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Vertex methods here!
      public List<Arc> getOutEdges() {
         List<Arc> returnable = new ArrayList<>();
         if(this.first == null) return null;

         for (Arc v = this.first; v != null; v = v.next) {
            returnable.add(v);
         }
         return returnable;
      }

      public void setVertexInfo(int i){
         this.info = i;
      }

      public int getVertexInfo(){
         return this.info;
      }

      public void setVertexDistance(int i){
         this.distance = i;
      }

      public int getVertexDistance(){
         return this.distance;
      }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Arc methods here!

      /**
       * A simple getter for returning the target vertex object.
       * @return target;
       */
      public Vertex getTargetVert() {
         return target;
      }
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /// / TODO!!! Your Graph methods here! Probably your solution belongs here.
      /**
       * Method for calculating the eccentricity of a graph vertex.
       * Used sources: http://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
       * @param tipp;
       * @return eccentricity;
       */
      public int breadthFirst(Vertex tipp){
         if (getVertexList() == null) throw new RuntimeException("No vertices defined!");
         if (!getVertexList().contains(tipp)) throw new RuntimeException("Wrong argument to traverse!");
         if (getVertexList().size() == 1) return 0;

         int eccentricity = 0; //Variable for keeping the eccentricity during iteration.

         Iterator vit = getVertexList().iterator(); //Vertices to be worked on
         while (vit.hasNext()) { //while there are vertices to work on
            Vertex v = (Vertex)vit.next(); //Returns the next element in the iteration
            v.setVertexInfo(0); //White - meaning they have not been worked on.
            v.setVertexDistance(0); //Default distance is 0;
         }
         List vq = Collections.synchronizedList (new LinkedList()); //List for adding vertices later on
         vq.add(tipp); //add the base vertex to the list previously defined.
         tipp.setVertexInfo(1); //Gray - meaning it is currently being worked on.
         while (vq.size() > 0) { //While there are still elements in vq;
            Vertex v = (Vertex)vq.remove(0); // take the element at index 0 from vq and give it's value to v;
            v.setVertexInfo(2); //Black - meaning working on it has been finished.
            Iterator eit = v.getOutEdges().iterator(); //get all of the edges connected to the vertex v
            while (eit.hasNext()) { //while there are elements left in the list of edges connected to vertex v;
               Arc a = (Arc)eit.next(); // cast element from eit as Arc
               Vertex w = a.getTargetVert(); //get vertex of the other side of the arc
               if (w.getVertexInfo() == 0) { // if vertex has not been worked on before, then:
                  w.setVertexDistance(v.getVertexDistance() + 1); // give the vertex the appropriate distance from our base vertex, meaning +1 step.
                  if (eccentricity < w.getVertexDistance()) // if the vertex's distance from base is greater than in any other vertex's case, then:
                     eccentricity = w.getVertexDistance(); // give its value to eccentricity.
                  vq.add(w); //add w to vq to be further processed.
                  w.setVertexInfo(1); //set w's status to "in progress".
               }
            }
         }

         return eccentricity;
      }

      /**
       * A method for assembling and returning the list of vertices of a graph.
       * @return returnable;
       */
      public ArrayList<Vertex> getVertexList() {
         ArrayList<Vertex> returnable = new ArrayList<>();

         for (Vertex v = this.first; v != null; v = v.next) {
            returnable.add(v);
         }
         return returnable;
      }
   }

   /**
    * Test graph 1
    */
   private void test1(){
      Vertex a = new Vertex("A");
      g.first = a;
   }

   /**
    * Test graph 2
    */
   private void test2(){
      Vertex a = new Vertex("A");
      Vertex b = new Vertex("B");

      Arc ab = new Arc("AB");
      Arc ba = new Arc("BA");

      g.first = a;
      a.next = b;

      a.first = ab;
      ab.target = b;

      b.first = ba;
      ba.target = a;
   }

   /**
    * Test graph 3
    */
   private void test3(){
      Vertex a = new Vertex("A");
      Vertex b = new Vertex("B");
      Vertex c = new Vertex("C");

      Arc ab = new Arc("AB");
      Arc ba = new Arc("BA");

      Arc bc = new Arc("BC");
      Arc cb = new Arc("CB");

      g.first = a;
      a.next = b;
      b.next = c;

      a.first = ab;
      ab.target = b;

      b.first = ba;
      ba.target = a;
      ba.next = bc;
      bc.target = c;

      c.first = cb;
      cb.target = b;
   }

   /**
    * Test graph 4
    */
   private void test4(){

      Vertex a = new Vertex("A");
      Vertex b = new Vertex("B");
      Vertex c = new Vertex("C");
      Vertex d = new Vertex("D");
      Vertex e = new Vertex("E");
      Vertex f = new Vertex("F");
      Vertex h = new Vertex("H");

      Arc ab = new Arc("AB");
      Arc ba = new Arc("BA");

      Arc bc = new Arc("BC");
      Arc cb = new Arc("CB");

      Arc bd = new Arc("BD");
      Arc db = new Arc("DB");

      Arc be = new Arc("BE");
      Arc eb = new Arc("EB");

      Arc bf = new Arc("BF");
      Arc fb = new Arc("FB");

      Arc fh = new Arc("FH");
      Arc hf = new Arc("HF");

      g.first = a;
      a.next = b;
      b.next = c;
      c.next = d;
      d.next = e;
      e.next = f;
      f.next = h;

      a.first = ab;
      ab.target = b;

      b.first = ba;
      ba.target = a;
      ba.next = bc;
      bc.target = c;
      bc.next = bd;
      bd.target = d;
      bd.next = be;
      be.target = e;
      be.next = bf;
      bf.target = f;

      c.first = cb;
      cb.target = b;

      d.first = db;
      db.target = b;

      e.first = eb;
      eb.target = b;

      f.first = fb;
      fb.target = b;
      fb.next = fh;
      fh.target = h;
      h.first = hf;
      hf.target = f;
   }

   /**
    * Test graph 5
    */
   private void test5(){
      Vertex a = new Vertex("A");
      Vertex b = new Vertex("B");
      Vertex c = new Vertex("C");
      Vertex d = new Vertex("D");
      Vertex e = new Vertex("E");
      Vertex f = new Vertex("F");
      Vertex h = new Vertex("H");

      Arc ab = new Arc("AB");
      Arc ba = new Arc("BA");

      Arc bc = new Arc("BC");
      Arc cb = new Arc("CB");

      Arc bd = new Arc("BD");
      Arc db = new Arc("DB");

      Arc be = new Arc("BE");
      Arc eb = new Arc("EB");

      Arc df = new Arc("DF");
      Arc fd = new Arc("FD");

      Arc eh = new Arc("EH");
      Arc he = new Arc("HE");

      g.first = a;
      a.next = b;
      b.next = c;
      c.next = d;
      d.next = e;
      e.next = f;
      f.next = h;

      a.first = ab;
      ab.target = b;

      b.first = ba;
      ba.target = a;
      ba.next = bc;
      bc.target = c;
      bc.next = bd;
      bd.target = d;
      bd.next = be;
      be.target = e;

      c.first = cb;
      cb.target = b;

      d.first = db;
      db.target = b;
      db.next = df;
      df.target = f;

      e.first = eb;
      eb.target = b;
      eb.next = eh;
      eh.target = h;

      f.first = fd;
      fd.target = d;

      h.first = he;
      he.target = e;
   }

   /**
    * Test graph 6
    */
   private void test6() {
      Vertex a = new Vertex("A");
      Vertex b = new Vertex("B");
      Vertex c = new Vertex("C");
      Vertex d = new Vertex("D");
      Vertex e = new Vertex("E");
      Vertex f = new Vertex("F");
      Vertex h = new Vertex("H");
      Vertex i = new Vertex("I");
      Vertex j = new Vertex("J");

      Arc ea = new Arc("EA");
      Arc ae = new Arc("AE");

      Arc fd = new Arc("FD");
      Arc df = new Arc("DF");

      Arc ih = new Arc("IH");
      Arc hi = new Arc("HI");

      Arc hb = new Arc("HB");
      Arc bh = new Arc("BH");

      Arc cj = new Arc("CJ");
      Arc jc = new Arc("JC");

      Arc ad = new Arc("AD");
      Arc da = new Arc("DA");

      Arc bc = new Arc("BC");
      Arc cb = new Arc("CB");

      Arc ab = new Arc("AB");
      Arc ba = new Arc("BA");

      Arc dc = new Arc("DC");
      Arc cd = new Arc("CD");

      g.first = a;
      a.next = b;
      b.next = c;
      c.next = d;
      d.next = e;
      e.next = f;
      f.next = h;
      h.next = i;
      i.next = j;

      e.first = ea;
      ea.target = a;

      a.first = ae;
      ae.target = e;
      ae.next = ad;
      ad.target = d;
      ad.next = ab;
      ab.target = b;

      f.first = fd;
      fd.target = d;

      d.first = df;
      df.target = f;
      df.next = da;
      da.target = a;
      da.next = dc;
      dc.target = c;

      j.first = jc;
      jc.target = c;

      c.first = cj;
      cj.target = j;
      cj.next = cd;
      cd.target = d;
      cd.next = cb;
      cb.target = b;

      i.first = ih;
      ih.target = h;

      h.first = hi;
      hi.target = i;
      hi.next = hb;
      hb.target = b;

      b.first = bh;
      bh.target = h;
      bh.next = ba;
      ba.target = a;
      ba.next = bc;
      bc.target = c;
   }
}